# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the krecorder package.
# Sergiu Bivol <sergiu@cip.md>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: krecorder\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-09 01:34+0000\n"
"PO-Revision-Date: 2022-04-30 11:32+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: audiorecorder.cpp:170
#, kde-format
msgid "Default file format"
msgstr ""

#: audiorecorder.cpp:179
#, fuzzy, kde-format
#| msgid "Audio Codec:"
msgid "Default audio codec"
msgstr "Codec audio:"

#: contents/ui/DefaultPage.qml:23 contents/ui/PlayerPage.qml:31
#: contents/ui/RecordingListPage.qml:45
#: contents/ui/settings/SettingsDialog.qml:14
#: contents/ui/settings/SettingsPage.qml:14
#: contents/ui/settings/SettingsWindow.qml:13
#, kde-format
msgid "Settings"
msgstr "Configurări"

#: contents/ui/DefaultPage.qml:35
#, fuzzy, kde-format
#| msgid "Click on a recording to play it, or record a new one"
msgid "Play a recording, or record a new one"
msgstr "Apăsați pe o înregistrare pentru a o reda, sau înregistrați una nouă"

#: contents/ui/DefaultPage.qml:35
#, fuzzy, kde-format
#| msgid "Save recording"
msgid "Record a new recording"
msgstr "Salvează înregistrarea"

#: contents/ui/main.qml:23
#, kde-format
msgid "Recorder"
msgstr "Înregistrator"

#: contents/ui/PlayerPage.qml:20
#, kde-format
msgid "Player"
msgstr ""

#: contents/ui/PlayerPage.qml:56
#, fuzzy, kde-format
#| msgid "Recorder"
msgid "Recorded on %1"
msgstr "Înregistrator"

#: contents/ui/PlayerPage.qml:82 contents/ui/RecordPage.qml:117
#, kde-format
msgid "Pause"
msgstr "Întrerupe"

#: contents/ui/PlayerPage.qml:82
#, kde-format
msgid "Play"
msgstr ""

#: contents/ui/PlayerPage.qml:92
#, kde-format
msgid "Stop"
msgstr "Oprește"

#: contents/ui/RecordingListDelegate.qml:75
#: contents/ui/RecordingListPage.qml:172
#, kde-format
msgid "Export to location"
msgstr ""

#: contents/ui/RecordingListDelegate.qml:83
#, kde-format
msgid "Rename"
msgstr ""

#: contents/ui/RecordingListDelegate.qml:91
#: contents/ui/RecordingListPage.qml:187 contents/ui/RecordPage.qml:194
#, kde-format
msgid "Delete"
msgstr "Șterge"

#: contents/ui/RecordingListPage.qml:22
#, kde-format
msgid "Recordings"
msgstr "Înregistrări"

#: contents/ui/RecordingListPage.qml:37 contents/ui/RecordingListPage.qml:178
#, kde-format
msgid "Edit"
msgstr "Modifică"

#: contents/ui/RecordingListPage.qml:51
#, kde-format
msgid "Record"
msgstr "Înregistrează"

#: contents/ui/RecordingListPage.qml:113
#, kde-format
msgid "No recordings"
msgstr "Nicio înregistrare"

#: contents/ui/RecordingListPage.qml:149
#, kde-format
msgid "Select a location to save recording %1"
msgstr ""

#: contents/ui/RecordingListPage.qml:159
#, fuzzy, kde-format
#| msgid "Save recording"
msgid "Saved recording to %1"
msgstr "Salvează înregistrarea"

#: contents/ui/RecordingListPage.qml:212
#, kde-format
msgid "Delete %1"
msgstr "Șterge %1"

#: contents/ui/RecordingListPage.qml:213
#, kde-format
msgid ""
"Are you sure you want to delete the recording %1?<br/>It will be "
"<b>permanently lost</b> forever!"
msgstr ""
"Sigur doriți să ștergeți înregistrarea %1?<br/>Aceasta se va <b>pierde "
"irecuperabil</b> pentru totdeauna!"

#: contents/ui/RecordingListPage.qml:217
#, kde-format
msgctxt "@action:button"
msgid "Delete"
msgstr "Șterge"

#: contents/ui/RecordingListPage.qml:228
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Renunță"

#: contents/ui/RecordingListPage.qml:240
#, kde-format
msgid "Rename %1"
msgstr ""

#: contents/ui/RecordingListPage.qml:255 contents/ui/RecordPage.qml:244
#, kde-format
msgid "Name:"
msgstr "Denumire:"

#: contents/ui/RecordingListPage.qml:261
#, kde-format
msgid "Location:"
msgstr "Amplasare:"

#: contents/ui/RecordPage.qml:21
#, kde-format
msgid "Record Audio"
msgstr "Înregistrează sunet"

#: contents/ui/RecordPage.qml:117
#, kde-format
msgid "Continue"
msgstr ""

#: contents/ui/RecordPage.qml:176
#, fuzzy, kde-format
#| msgid "Save recording"
msgid "Save Recording"
msgstr "Salvează înregistrarea"

#: contents/ui/RecordPage.qml:214
#, kde-format
msgid "Save recording"
msgstr "Salvează înregistrarea"

#: contents/ui/RecordPage.qml:218
#, kde-format
msgid "Save"
msgstr "Salvează"

#: contents/ui/RecordPage.qml:230
#, kde-format
msgid "Discard"
msgstr "Aruncă"

#: contents/ui/RecordPage.qml:245
#, kde-format
msgid "Name (optional)"
msgstr "Denumire (opțional)"

#: contents/ui/RecordPage.qml:249
#, kde-format
msgid "Storage Folder:"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:34
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:40
#, kde-format
msgid "About"
msgstr "Despre"

#: contents/ui/settings/SettingsComponent.qml:58
#, fuzzy, kde-format
#| msgid "Audio Input:"
msgid "Audio Input"
msgstr "Intrare audio:"

#: contents/ui/settings/SettingsComponent.qml:125
#, kde-format
msgid "Advanced"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:132
#, fuzzy, kde-format
#| msgid "Audio Codec:"
msgid "Audio Codec"
msgstr "Codec audio:"

#: contents/ui/settings/SettingsComponent.qml:165
#, fuzzy, kde-format
#| msgid "Container Format:"
msgid "Container Format"
msgstr "Format container:"

#: contents/ui/settings/SettingsComponent.qml:198
#, fuzzy, kde-format
#| msgid "Audio Quality:"
msgid "Audio Quality"
msgstr "Calitate audio:"

#: contents/ui/settings/SettingsComponent.qml:199
#, kde-format
msgid "Lowest"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:199
#, kde-format
msgid "Low"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:199
#, kde-format
msgid "Medium"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:199
#, kde-format
msgid "High"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:199
#, kde-format
msgid "Highest"
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:200
#, kde-format
msgid "Higher audio quality also increases file size."
msgstr ""

#: contents/ui/settings/SettingsComponent.qml:225
#, kde-format
msgid ""
"Some combinations of codecs and container formats may not be compatible."
msgstr ""

#: main.cpp:51
#, fuzzy, kde-format
#| msgid "© 2020-2022 KDE Community"
msgid "© 2020-2024 KDE Community"
msgstr "© 2020-2022 Comunitatea KDE"

#: main.cpp:52
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:53
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"

#, fuzzy
#~| msgid "Recordings"
#~ msgid "Stop Recording"
#~ msgstr "Înregistrări"

#, fuzzy
#~| msgid "Format:"
#~ msgid "Audio Format"
#~ msgstr "Format:"

#, fuzzy
#~| msgid "Ogg Vorbis"
#~ msgctxt "Ogg Vorbis file format"
#~ msgid "Ogg Vorbis"
#~ msgstr "Ogg Vorbis"

#, fuzzy
#~| msgid "Ogg Opus"
#~ msgctxt "Ogg Opus file format"
#~ msgid "Ogg Opus"
#~ msgstr "Ogg Opus"

#, fuzzy
#~| msgid "FLAC"
#~ msgctxt "FLAC file format"
#~ msgid "FLAC"
#~ msgstr "FLAC"

#, fuzzy
#~| msgid "MP3"
#~ msgctxt "MP3 file format"
#~ msgid "MP3"
#~ msgstr "MP3"

#~ msgid "WAV"
#~ msgstr "WAV"

#~ msgid "No recordings yet, record your first!"
#~ msgstr "Nicio înregistrare, creați-o pe prima!"

#~ msgid "Editing %1"
#~ msgstr "Se modifică %1"

#~ msgid "Audio Input:"
#~ msgstr "Intrare audio:"

#~ msgid "<b>New Recording</b>"
#~ msgstr "<b>Înregistrare nouă</b>"

#~ msgid "<b>Settings</b>"
#~ msgstr "<b>Configurări</b>"

#~ msgid "Hide Advanced Settings"
#~ msgstr "Ascunde configurările avansate"

#~ msgid "Show Advanced Settings"
#~ msgstr "Arată configurările avansate"
